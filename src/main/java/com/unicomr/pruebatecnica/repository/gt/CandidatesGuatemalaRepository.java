package com.unicomr.pruebatecnica.repository.gt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unicomr.pruebatecnica.entities.Candidate;

@Repository
public interface CandidatesGuatemalaRepository extends JpaRepository<Candidate, Long>{

}
