package com.unicomr.pruebatecnica.repository.jm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unicomr.pruebatecnica.entities.Candidate;

@Repository
public interface CandidatesJamaicaRepository extends JpaRepository<Candidate, Long>{

}
