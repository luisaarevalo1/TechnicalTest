package com.unicomr.pruebatecnica.repository.cr;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unicomr.pruebatecnica.entities.Candidate;

@Repository
public interface CandidatesCostaRicaRepository extends JpaRepository<Candidate, Long>{

}
