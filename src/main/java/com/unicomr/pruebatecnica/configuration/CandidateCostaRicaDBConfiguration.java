package com.unicomr.pruebatecnica.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "crEntityManagerFactory", 
	basePackages= {"com.unicomr.pruebatecnica.repository.cr"}, transactionManagerRef="crTransactionManager")
public class CandidateCostaRicaDBConfiguration {
	@Bean(name="crDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.cr")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name="crEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder builder, 
			@Qualifier("crDataSource") DataSource dataSource) {
		
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		
		return builder
				.dataSource(dataSource)
				.properties(properties)
				.packages("com.unicomr.pruebatecnica.entities")
				.persistenceUnit("Candidate")
				.build();
	}
	@Bean(name="crTransactionManager")
	public PlatformTransactionManager transactionManager(@Qualifier("crEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
