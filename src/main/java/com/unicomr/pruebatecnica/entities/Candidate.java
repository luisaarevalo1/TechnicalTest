package com.unicomr.pruebatecnica.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Candidate {
	private String country;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long customerId;
	private String firstName;
	private String lastName;
	private String birthday;
	private String gender;
	private String cellphone;
	private String homePhone;
	private String address;
	private String profession;
	private String incomes;
}
