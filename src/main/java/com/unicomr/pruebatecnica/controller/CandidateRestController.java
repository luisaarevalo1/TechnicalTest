package com.unicomr.pruebatecnica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unicomr.pruebatecnica.entities.Candidate;
import com.unicomr.pruebatecnica.repository.cr.CandidatesCostaRicaRepository;
import com.unicomr.pruebatecnica.repository.gt.CandidatesGuatemalaRepository;
import com.unicomr.pruebatecnica.repository.jm.CandidatesJamaicaRepository;
import com.unicomr.pruebatecnica.service.CandidateService;

@RestController
@RequestMapping("/candidate")
public class CandidateRestController {
	@Autowired
	CandidateService candidateService;
	
	@GetMapping()
	public List<Candidate> findAll(@RequestHeader("country") String country){
		return candidateService.getAll(country);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> post(@RequestBody Candidate candidate, @RequestHeader("country") String country){
		Candidate c = candidateService.saveCandidate(candidate, country);
		return ResponseEntity.ok(c);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody Candidate candidate, @RequestHeader("country") String country){
		Candidate c = candidateService.updateCandidate(candidate, country);
		return ResponseEntity.ok(c);
	}
}
