package com.unicomr.pruebatecnica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unicomr.pruebatecnica.entities.Candidate;
import com.unicomr.pruebatecnica.repository.cr.CandidatesCostaRicaRepository;
import com.unicomr.pruebatecnica.repository.gt.CandidatesGuatemalaRepository;
import com.unicomr.pruebatecnica.repository.jm.CandidatesJamaicaRepository;

@Service
public class CandidateService {
	@Autowired
	CandidatesCostaRicaRepository candidatesCostaRicaRepository;
	
	@Autowired
	CandidatesGuatemalaRepository candidatesGuatemalaRepository;
	
	@Autowired
	CandidatesJamaicaRepository candidatesJamaicaRepository;
	
	public Candidate saveCandidate(Candidate candidate, String country) {
		if("CR".equalsIgnoreCase(country)) {
			return candidatesCostaRicaRepository.save(candidate);
		}else if("GT".equalsIgnoreCase(country)) {
			return candidatesGuatemalaRepository.save(candidate);
		}else if("JM".equalsIgnoreCase(country)) {
			return candidatesJamaicaRepository.save(candidate);
		}
		else {
			throw new IllegalArgumentException("Invalid country");
		}
	}
	
	public List<Candidate> getAll(String country) {
		if("CR".equalsIgnoreCase(country)) {
			return candidatesCostaRicaRepository.findAll();
		}else if("GT".equalsIgnoreCase(country)) {
			return candidatesGuatemalaRepository.findAll();
		}else if("JM".equalsIgnoreCase(country)) {
			return candidatesJamaicaRepository.findAll();
		}
		else {
			throw new IllegalArgumentException("Invalid country");
		}
	}
	
	public Candidate updateCandidate(Candidate candidate, String country) {
		if("CR".equalsIgnoreCase(country)) {
			if(candidatesCostaRicaRepository.existsById(candidate.getCustomerId())) {
				return candidatesCostaRicaRepository.save(candidate);
			}else {
				throw new IllegalArgumentException("Candidate does not exist");
			}
		}else if("GT".equalsIgnoreCase(country)) {
			if(candidatesGuatemalaRepository.existsById(candidate.getCustomerId())) {
				return candidatesGuatemalaRepository.save(candidate);
			}else {
				throw new IllegalArgumentException("Candidate does not exist");
			}
		}else if("JM".equalsIgnoreCase(country)) {
			if(candidatesJamaicaRepository.existsById(candidate.getCustomerId())) {
				return candidatesJamaicaRepository.save(candidate);
			}else {
				throw new IllegalArgumentException("Candidate does not exist");
			}
		}
		else {
			throw new IllegalArgumentException("Invalid country");
		}
	}
}
