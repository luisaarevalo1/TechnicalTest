Technical Test - UNICOMER
Service which is available for Jamaica, Guatemala and Costa Rica to have the information of candidates.

I could not deploy the project but if you run it locally this is the URL to access Swagger: http://localhost:8080/swagger-ui/index.html#/

To access to the database in Swagger to do the GET, POST and UPDATE is necessary that in the header is sent the country if the country is not set, it won't let you do nothing

GT - For Guatemala
CR - For Costa Rica
JM - For Jamaica

Thanks for the opportunity! 
Regards
Luisa Arevalo